import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ModalComponent from "@/components/ModalComponent.vue";
Vue.config.productionTip = false
Vue.component('ModalComponent', ModalComponent);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
