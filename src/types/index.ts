/**
 * Типа для задачи
 */
export type TaskType = {
  id: number;
  name: string;
  complete: boolean;
}

/**
 * Интерфейс для заметки
 */
export interface InterfaceToDo {
  id: number;
  name: string;
  tasks: Array<TaskType>;
  add(id: number, name: string, complete: boolean): void;
  remove(id: number): void;
}

/**
 * Класс заметки с реализацией интерфейса 
 */
export class ToDo implements InterfaceToDo {
  id: number;
  name: string;
  tasks: Array<TaskType> = [];

  constructor(id: number, name: string, tasks?: Array<TaskType>) {
    this.id = id;
    this.name = name;
    this.tasks = tasks || []
  }

  /**
   * 
   * @param id - ид-заметки
   * @param name - имя
   * @param complete - статус выполнения
   */
  add(id: number, name: string, complete: boolean): TaskType {
    const newItem: TaskType = { id: id, name: name, complete: complete };
    this.tasks.push(newItem);
    return newItem;
  }

  remove(id: number): void {
    this.tasks = this.tasks.filter(t => t.id !== id);
  }
}