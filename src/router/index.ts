import Vue from 'vue'
import VueRouter from 'vue-router'
import ToDoList from '../components/ToDoList.vue'

Vue.use(VueRouter)
const routes = [
  {
    name: 'home',
    path: '/', component: ToDoList,
    children: [
      {
        name: 'add',
        path: 'add-todo',
        component: () => import('../components/AddEditToDo.vue')
      },
      {
        name: 'edit',
        path: 'edit-todo/:id',
        component: () => import('../components/AddEditToDo.vue')
      }
    ]
  },
];


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
