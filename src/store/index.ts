import Vue from 'vue'
import Vuex from 'vuex'
import { ToDo } from '../types/index'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todoList: Array<ToDo>()
  },
  getters: {
    getToDoList: (state): Array<ToDo> => {
      return state.todoList;
    },
    doneTodos: (state, toDoItem: ToDo) => {
      return toDoItem.tasks.filter(item => item.complete == true).length;
    }
  },
  mutations: {
    addToDo(state, todoModel: ToDo) {
      const exists: ToDo = state.todoList.find(i => i.id === todoModel.id) as ToDo;
      if (exists) {
        todoModel.id = new Date().getTime()
        Vue.set(state.todoList, state.todoList.indexOf(exists), todoModel)
      } else {
        state.todoList.push(todoModel);
      }
    },
    removeToDo(state, id: number) {
      state.todoList = state.todoList.filter(i => i.id !== id);
    },
    loadLocalStorage(state) {
      if (localStorage.getItem('store')) {
        try {
          state.todoList = JSON.parse(window.localStorage.getItem('store') as string);
        } catch (error) {
          throw new Error(error);
        }
      }
    },
    saveLocalStorage(state) {
      window.localStorage.setItem('store', JSON.stringify(state.todoList))
    }
  },
  actions: {
    loadLocalStorage(context) {
      context.commit('loadLocalStorage');
    },
    addToDo(context, todoModel: ToDo) {
      context.commit('addToDo', todoModel);
      context.commit('saveLocalStorage');
      
    },
    removeToDo(context, id: number) {
      context.commit('removeToDo', id);
      context.commit('saveLocalStorage');
    }
  },
  modules: {
  }
})
